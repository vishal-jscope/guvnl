var crypto = require('crypto');
const hash = function(password) {
				  return crypto.createHash('sha256').update(password).digest('hex');
				}
module.exports = hash;