module.exports = function(req, res, next) {
	if( typeof req.session.user_id === 'undefined') {	
		res.redirect('/');
	}
  	else {  
  		next();
  	}
}	