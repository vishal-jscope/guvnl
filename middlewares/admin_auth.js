module.exports = function(req, res, next) {
	if( typeof req.session.admin_id === 'undefined') {	
		res.redirect('/');
	}
  	else {  		
   		next();
  	}
}	