var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var flash  = require('express-flash');



var index = require('./routes/index');
var users = require('./routes/users');
var admin = require('./routes/admin');

var app = express();


//app.use(express.cookieParser()); 
app.use(session({
	secret: '223bfaaeb35ab62dd321ed83541236f42a67e67a8577409ac35a8118d5227485',
  resave: false,
  saveUninitialized: true
}));
app.use(flash());


app.locals.hash =  require('./helpers/hash');
app.locals.moment =  require('moment');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(require('express-method-override')('_method'));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/admin', admin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

ADMIN_URL = 'http://localhost:3000/admin/';

app.use(function(req, res, next) {
console.log(req.url);

  });

module.exports = app;

