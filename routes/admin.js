var express = require('express');
var router = express.Router();
var auth = require("../middlewares/admin_auth");
var home = require('../app/controllers/admin/HomeController');
var user = require('../app/controllers/admin/UserController');
var team = require('../app/controllers/admin/TeamController');
var project = require('../app/controllers/admin/ProjectController');
/* GET  listing. */

router.get('/', auth, home.index);

router.get('/users', auth, user.index);
router.get('/users/create', auth, user.create);
router.post('/users', auth, user.store);
router.get('/users/edit/:id', auth, user.edit);
router.put('/users', auth, user.update);
router.delete('/users', auth, user.delete);

router.get('/teams', auth, team.index);
router.get('/teams/create', auth, team.create);
router.post('/teams', auth, team.store);
router.get('/teams/edit/:id', auth, team.edit);
router.put('/teams', auth, team.update);
router.delete('/teams', auth, team.delete);

router.get('/projects', auth, project.index);
router.get('/projects/create', auth, project.create);
router.post('/projects', auth, project.store);
router.get('/projects/edit/:id', auth, project.edit);
router.put('/projects', auth, project.update);
router.delete('/projects', auth, project.delete);

router.get('/logout',function(req, res, next){
	delete req.session.admin_id;
	res.redirect('/');
})
module.exports = router;
