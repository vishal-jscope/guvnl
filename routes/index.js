var express = require('express');
var router = express.Router();
var auth = require("../middlewares/auth");
var user = require('../app/controllers/UserController');

/* GET home page. */
router.get('/', user.index);
router.post('/login', user.login);
router.get('/logout', user.logout);
router.get('/dashboard', auth, function(req,res,next){
	res.render('dashboard.ejs');
});
module.exports = router;
