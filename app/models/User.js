var qb = require("../../config/database.js");
const userTable = "users";

exports.getLogin = function(data,done) {
 
    qb.select('*')
      .where({email: data.email, password: data.password,status: data.status})
      .get(userTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );

}