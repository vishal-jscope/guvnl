var qb = require("../../../config/database.js");
const projectTable = "projects";
const teamTable = "teams";
const userTable = "users";
const projectUsersTable = "project_users";
exports.getAll = function(done) {
 
    qb.select('*')
      .get(projectTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );

}

exports.store = function(data,done) {
  qb.insert(projectTable, data, (err, response) => {
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response.insertId);
        }
  });
}

exports.get = function(id,done) {
 
    qb.select('*')
      .where({id: id})
      .get(projectTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );
}

exports.update = function(data,id,done) {
  qb.update(projectTable, data,{id:id}, (err, response) => {
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response);
        }
  });
}

exports.delete = function(id,done) {
  qb.delete(projectTable,{id:id}, (err, response) => {
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response);
        }
  });
}

exports.checkNameExist = function(id,name,done) { 
    if(id != '') {
        qb.select('*')
          .where('id !=',id)    
          .where({name: name})
          .get(projectTable, (err,response) => {
            console.log("Query Ran: " + qb.last_query());
            //console.log(err);
           // console.log(response);
                if (err){
                    //console.log(err);
                    done(err,null);
                }
                else{
                    //console.log(response);
                    done(null,response);
                }          
            }
          );
    }
    else {      
        qb.select('*')
        .where({name: name})
        .get(projectTable, (err,response) => {
          //console.log("Query Ran: " + qb.last_query());
          //console.log(err);
         // console.log(response);
              if (err){
                  //console.log(err);
                  done(err,null);
              }
              else{
                  //console.log(response);
                  done(null,response);
              }          
          }
        );
    }
}

exports.getAllUsers = function(done) {
 
    qb.select('*')
      .where('user_type =',1)
      .where('status =',1)
      .get(userTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );

}

exports.getAllTeams = function(done) {
 
    qb.select('*')
      .where('status =',1)
      .get(teamTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );

}

exports.projectUsersStore = function(data,done) {
  qb.insert(projectUsersTable, data, (err, response) => {
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response.insertId);
        }
  });
}

exports.getSelectedProjectUsers = function(id,done) {
 
    qb.select('*')
      .where({project_id: id})
      .get(projectUsersTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );
}

exports.deleteProjectUsers = function(id,done) {
  qb.delete(projectUsersTable,{project_id:id}, (err, response) => {
      console.log("Query Ran: " + qb.last_query());
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response);
        }
  });
}