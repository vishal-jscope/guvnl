var qb = require("../../../config/database.js");
const teamTable = "teams";
const userTable = "users";
const teamUsersTable = "team_users";
exports.getAll = function(done) {
 
    qb.select('*')
      .get(teamTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );

}

exports.store = function(data,done) {
  qb.insert(teamTable, data, (err, response) => {
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response.insertId);
        }
  });
}

exports.get = function(id,done) {
 
    qb.select('*')
      .where({id: id})
      .get(teamTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );
}

exports.update = function(data,id,done) {
  qb.update(teamTable, data,{id:id}, (err, response) => {
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response);
        }
  });
}

exports.delete = function(id,done) {
  qb.delete(teamTable,{id:id}, (err, response) => {
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response);
        }
  });
}

exports.checkNameExist = function(id,name,done) { 
    if(id != '') {
        qb.select('*')
          .where('id !=',id)    
          .where({name: name})
          .get(teamTable, (err,response) => {
            console.log("Query Ran: " + qb.last_query());
            //console.log(err);
           // console.log(response);
                if (err){
                    //console.log(err);
                    done(err,null);
                }
                else{
                    //console.log(response);
                    done(null,response);
                }          
            }
          );
    }
    else {      
        qb.select('*')
        .where({name: name})
        .get(teamTable, (err,response) => {
          //console.log("Query Ran: " + qb.last_query());
          //console.log(err);
         // console.log(response);
              if (err){
                  //console.log(err);
                  done(err,null);
              }
              else{
                  //console.log(response);
                  done(null,response);
              }          
          }
        );
    }
}

exports.getAllUsers = function(done) {
 
    qb.select('*')
      .where('user_type =',1)
      .where('status =',1)
      .get(userTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );

}

exports.teamUsersStore = function(data,done) {
  qb.insert(teamUsersTable, data, (err, response) => {
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response.insertId);
        }
  });
}

exports.getSelectedTeamUsers = function(id,done) {
 
    qb.select('*')
      .where({team_id: id})
      .get(teamUsersTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );
}

exports.deleteTeamUsers = function(id,done) {
  qb.delete(teamUsersTable,{team_id:id}, (err, response) => {
      console.log("Query Ran: " + qb.last_query());
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response);
        }
  });
}