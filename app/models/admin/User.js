var qb = require("../../../config/database.js");
const userTable = "users";

exports.getAll = function(done) {
 
    qb.select('*')
      .where({user_type: 1})
      .get(userTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );

}

exports.store = function(data,done) {
  qb.insert(userTable, data, (err, response) => {
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response.insertId);
        }
  });
}

exports.get = function(id,done) {
 
    qb.select('*')
      .where({id: id})
      .get(userTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );
}

exports.update = function(data,id,done) {
  qb.update(userTable, data,{id:id}, (err, response) => {
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response);
        }
  });
}

exports.delete = function(id,done) {
  qb.delete(userTable,{id:id}, (err, response) => {
        if (err){
           done(err,null);
        }
        else{
            //console.log(response);
            done(null,response);
        }
  });
}

exports.checkEmailExist = function(email,done) {
 
    qb.select('*')
      .where({email: email})
      .get(userTable, (err,response) => {
        //console.log("Query Ran: " + qb.last_query());
        //console.log(err);
       // console.log(response);
            if (err){
                //console.log(err);
                done(err,null);
            }
            else{
                //console.log(response);
                done(null,response);
            }          
        }
      );
}