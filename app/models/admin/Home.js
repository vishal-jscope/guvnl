var qb = require("../../../config/database.js");
exports.countRecords = function(table,done) { 
  qb.count(table, (err, count) => {
     if (err){
          done(err,null);
      }
      else{
          done(null,count);
      }
});
}