var user = require('../models/User');

exports.index = function(req,res,next) {

	if( typeof req.session.user_id !== 'undefined') {
		res.redirect('/dashboard');
	}
	else if(typeof req.session.admin_id !== 'undefined'){
		res.redirect('/admin');
	}
	else{
		res.render('index.ejs',{});
	}	
	
}

exports.login = function(req,res,next) {	
	if(req.method == 'POST'){
		if(req.body.submit == 'Login'){
			//console.log(req.body);
			//var crypto = require('crypto');
			//const hash = function(password) {
			//	  return crypto.createHash('sha256').update(password).digest('hex')
			//	}
			//	console.log(hash(req.body.password));	
				const data = {
						        email: req.body.email,
						        password: req.app.locals.hash(req.body.password),
						        status: 1, // 0=Inactive, 1 = Active
						    };
				user.getLogin(data, function (err,result) {
					if(err){
						//console.log(err);
						req.flash('error', 'Something went wrong. Please try again later.');
						res.redirect('/');		
					}
					else{
							if(result.length > 0){						
								
								if(result[0].user_type == 1){ // 1 = User, 2 = admin 
									req.session.user_id = result[0].id;
									res.redirect('/dashboard');	
								}
								else{
									req.session.admin_id = result[0].id;
									res.redirect('/admin');
								}
							}
							else{								
								req.flash('error', 'Invalid Email or Password');
								res.redirect('/');		
							}						
					}					
					
				});

		}		
	}	
}
exports.logout = function(req,res,next) {
	delete req.session.user_id;
	res.redirect('/');
}