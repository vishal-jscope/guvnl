var user = require('../../models/admin/User');

exports.index = function(req,res,next) {

		user.getAll(function (err,result) {
			if(err){
				//console.log(err);
				//req.flash('error', 'Something went wrong. Please try again later.');
				res.render('admin/users/index.ejs',{
					error : req.flash('Something went wrong. Please try again later.')
				});	
			}
			else{
				
				var final_result = [];
					result.forEach(function(data,index){
						final_result[index] = {
												id         : data.id,
												name       : data.name,
												email      : data.email,
												status     : data.status,
												created_at : req.app.locals.moment(data.created_at).format('DD-MM-YYYY'),
												updated_at : req.app.locals.moment(data.updated_at).format('DD-MM-YYYY')
											};

					});
				res.render('admin/users/index.ejs',{
					page : 'users',
					result : final_result
				});				
			}					
			
		});			
}

exports.create = function(req,res,next) {
		res.render('admin/users/create.ejs',{
			page : 'users'
		});	
}

exports.store = function(req,res,next) {
	
	if(req.method == 'POST'){
		if(req.body.submit == 'Add User'){
				console.log(req.body);
				// check email exist
				user.checkEmailExist(req.body.email, function (err,result) {
					if(err){
						//console.log(err);
						req.flash('error','Something went wrong. Please try again later.');
						res.redirect(ADMIN_URL+'users/create');		
					}
					else{
							if(result.length > 0){
								req.flash('error','Email already exist.');
								res.redirect(ADMIN_URL+'users/create');								
							}
							else{
								var status = 0;
								if(req.body.status == 'on'){
									status = 1;
								}
								const data = {
												name: req.body.name,
										        email: req.body.email,
										        password: req.app.locals.hash(req.body.password),
										        status: status,
										        created_at: req.app.locals.moment().format('YYYY-MM-DD h:m:s'),
										        updated_at: req.app.locals.moment().format('YYYY-MM-DD h:m:s')
										    };
								user.store(data, function (err,result) {
									if(err){
										//console.log(err);
										req.flash('error', 'Something went wrong. Please try again later.');
										res.redirect(ADMIN_URL+'users');		
									}
									else{
											req.flash('success', 'User added successfully.');

											res.redirect(ADMIN_URL+'users');					
									}					
									
								});
							}				
					}					
					
				});

		}		
	}	
}

exports.edit = function(req,res,next) {
		var id = req.params.id;
		if(id == 0 || id == ''){
			res.redirect(ADMIN_URL+'users');
		}
		user.get(id, function (err,result) {
			if(err){
				//console.log(err);
				req.flash('error', 'Something went wrong. Please try again later.');
				res.redirect(ADMIN_URL+'users');		
			}
			else{
									
					var final_result = [];
					result.forEach(function(data,index){
						final_result =  {
											id         : data.id,
											name       : data.name,
											email      : data.email,
											status      : data.status
										};

					});
					
					res.render('admin/users/edit.ejs',{
						page : 'users',
						result : final_result
					});						
			}			
		});	
}

exports.update = function(req,res,next) {
	
	if(req.method == 'PUT'){
		if(req.body.submit == 'Edit User'){
				var id = req.body.id;
				var status = 0;
				if(req.body.status == 'on'){
					status = 1;
				}
				const data = {
								name: req.body.name,
								status: status,
						        updated_at: req.app.locals.moment().format('YYYY-MM-DD h:m:s')
						    };
				user.update(data,id, function (err,result) {
					if(err){
						//console.log(err);
						req.flash('error', 'Something went wrong. Please try again later.');
						res.redirect(ADMIN_URL+'users');		
					}
					else{
							req.flash('success', 'User updated successfully.');
							res.redirect(ADMIN_URL+'users');					
					}					
					
				});

		}	
	}	
}

exports.delete = function(req,res,next) {
		console.log(req.body);
	if(req.method == 'DELETE'){
		
				var id = req.body.id;
				user.delete(id, function (err,result) {
					if(err){
						//console.log(err);
						req.flash('error', 'Something went wrong. Please try again later.');
						res.redirect(ADMIN_URL+'users');		
					}
					else{
							req.flash('success', 'User deleted successfully.');
							res.redirect(ADMIN_URL+'users');					
					}					
					
				});
			
	}	
}