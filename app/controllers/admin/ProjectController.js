var project = require('../../models/admin/Project');
var async = require('async');
exports.index = function(req,res,next) {

		project.getAll(function (err,result) {
			if(err){
				//console.log(err);
				//req.flash('error', 'Something went wrong. Please try again later.');
				res.render('admin/projects/index.ejs',{
					error : req.flash('Something went wrong. Please try again later.')
				});	
			}
			else{
				
				var final_result = [];
					result.forEach(function(data,index){
						final_result[index] = {
												id         : data.id,
												name       : data.name,
												status     : data.status,
												created_at : req.app.locals.moment(data.created_at).format('DD-MM-YYYY'),
												updated_at : req.app.locals.moment(data.updated_at).format('DD-MM-YYYY')
											};

					});
				res.render('admin/projects/index.ejs',{
					page : 'projects',
					result : final_result
				});				
			}					
			
		});			
}

exports.create = function(req,res,next) {
		/*project.getAllUsers(function (err,result) {
			if(err){
				//console.log(err);
				//req.flash('error', 'Something went wrong. Please try again later.');
				res.render('admin/projects/index.ejs',{
					error : req.flash('Something went wrong. Please try again later.')
				});	
			}
			else{				
				var final_result = [];
					result.forEach(function(data,index){
						final_result[index] = {
											id         : data.id,
											name       : data.name,
											email      : data.email,
										};

					});
				console.log(final_result);	
				res.render('admin/projects/create.ejs',{
					users : final_result
				});				
			}					
			
		});*/

		async.parallel([
		 	function(callback) { project.getAllUsers(callback) },
		 	function(callback) { project.getAllTeams(callback) }
		], function(err, results) {

			var users = [];
			results[0].forEach(function(data,index){
				users[index] = {
									id         : data.id,
									name       : data.name,
									email      : data.email
								};

			});

			var teams = [];
			results[1].forEach(function(data,index){
				teams[index] = {
									id         : data.id,
									name       : data.name
								};

			});			
			
			res.render('admin/projects/create.ejs',{
						page : 'projects',
						users : users,
						teams : teams
						});		 
		});				
		
}

exports.store = function(req,res,next) {
	
	if(req.method == 'POST'){
		if(req.body.submit == 'Add Project'){
	 
				// check name exist
				project.checkNameExist('',req.body.name, function (err,result) {
					if(err){
						//console.log(err);
						req.flash('error','Something went wrong. Please try again later.');
						res.redirect(ADMIN_URL+'projects/create');		
					}
					else{
							if(result.length > 0){
								req.flash('error','Project already exist.');
								res.redirect(ADMIN_URL+'projects/create');								
							}
							else{
								var status = 0;
								if(req.body.status == 'on'){
									status = 1;
								}
																
								const data = {
												name: req.body.name,
												description: req.body.description,
												assignee:  req.body.assignee,
										        status: status,
										        created_at: req.app.locals.moment().format('YYYY-MM-DD h:m:s'),
										        updated_at: req.app.locals.moment().format('YYYY-MM-DD h:m:s')
										    };
								project.store(data, function (err,result) {
									if(err){
										//console.log(err);
										req.flash('error', 'Something went wrong. Please try again later.');
										res.redirect(ADMIN_URL+'projects');		
									}
									else{
											if( req.body.assignee == 1){ // users
												var user_ids = req.body.users;											
												if(user_ids.length > 0){
													user_ids.forEach(function(user_id,index){
														const  u_data = {
																			project_id: result,
																	        user_id: user_id,
																	    }
													    project.projectUsersStore(u_data, function (err,result) {});														
													});													
												}	
											}
											else{ // team
												const  u_data = {
																	project_id: result,
															        team_id: req.body.team,
															    }
												project.projectUsersStore(u_data, function (err,result) {});
											}

											

											req.flash('success', 'Project added successfully.');
											res.redirect(ADMIN_URL+'projects');					
									}					
									
								});
							}				
					}					
					
				});

		}		
	}	
}

exports.edit = function(req,res,next) {
		var id = req.params.id;
		if(id == 0 || id == ''){
			res.redirect(ADMIN_URL+'projects');
		}

		/*project.get(id, function (err,result) {
			if(err){
				//console.log(err);
				req.flash('error', 'Something went wrong. Please try again later.');
				res.redirect(ADMIN_URL+'projects');		
			}
			else{
									
					var final_result = {};
					result.forEach(function(data,index){
						final_result =  {
											id         : data.id,
											name       : data.name,
											status      : data.status
										};
					});
					
					project.getprojectUsers(id, function (err,result) {

						console.log(final_result);
						console.log(result);
						res.render('admin/projects/edit.ejs',{
						result : final_result,
						selected_result : result
						});

					});											
			}			
		});*/

		async.parallel([
		 	function(callback) { project.get(id, callback) },
		 	function(callback) { project.getAllUsers(callback) },
		 	function(callback) { project.getAllTeams(callback) },		 	
		  	function(callback) { project.getSelectedProjectUsers(id, callback) }
		], function(err, results) {

			var final_result = [];
			results[0].forEach(function(data,index){
				final_result =  {
									id         	: data.id,
									name       	: data.name,
									description : data.description,
									assignee 	: data.assignee,
									status      : data.status
								};
			});

			var users = [];
			results[1].forEach(function(data,index){
				users[index] = {
									id         : data.id,
									name       : data.name,
									email      : data.email,
								};

			});

			var teams = [];
			results[2].forEach(function(data,index){
				teams[index] = {
									id         : data.id,
									name       : data.name
								};
			});

			var selected_users = [];
			results[3].forEach(function(data,index){
				selected_users[index] = data.user_id;

			});

			var selected_team = [];
			results[3].forEach(function(data,index){
				selected_team = data.team_id;

			});
			
			console.log(selected_users);
			res.render('admin/projects/edit.ejs',{
						page : 'projects',
						result : final_result,
						users : users,
						teams : teams,
						selected_users : selected_users,
						selected_team : selected_team
						});
		 
		});	
}


exports.update = function(req,res,next) {
	
	if(req.method == 'PUT'){
		if(req.body.submit == 'Edit Project'){

				var id = req.body.id;

				// check name exist
				project.checkNameExist(id,req.body.name, function (err,result) {
					if(err){
						//console.log(err);
						req.flash('error','Something went wrong. Please try again later.');
						res.redirect(ADMIN_URL+'projects/create');		
					}
					else{
							if(result.length > 0){
								req.flash('error','Project already exist.');
								res.redirect(ADMIN_URL+'projects/edit/'+id);								
							}
							else{
								var status = 0;
								if(req.body.status == 'on'){
									status = 1;
								}
															
								const data = {
												name: req.body.name,
												description: req.body.description,
												assignee: req.body.assignee,
										        status: status,
										        updated_at: req.app.locals.moment().format('YYYY-MM-DD h:m:s')
										    };
								project.update(data,id, function (err,result) {
									if(err){
										//console.log(err);
										req.flash('error', 'Something went wrong. Please try again later.');
										res.redirect(ADMIN_URL+'projects');		
									}
									else{
											if( req.body.assignee == 1){ // users
												var user_ids = req.body.users;
											
												if(user_ids.length > 0){
													project.deleteProjectUsers(id, function (err,result) {
														user_ids.forEach(function(user_id,index){
															const  u_data = {
																				project_id: id,
																		        user_id: user_id,
																		    }
													   		 project.projectUsersStore(u_data, function (err,result) {});														
														});
													});												
												}
											}
											else{ // team
												project.deleteProjectUsers(id, function (err,result) {
													const  u_data = {
																		project_id: id,
																        team_id: req.body.team,
																    }
													project.projectUsersStore(u_data, function (err,result) {});
												});

											}

											req.flash('success', 'Project updated successfully.');
											res.redirect(ADMIN_URL+'projects');					
									}					
									
								});
							}				
					}					
					
				});


		}	
	}	
}

exports.delete = function(req,res,next) {
		console.log(req.body);
	if(req.method == 'DELETE'){
		
				var id = req.body.id;
				project.delete(id, function (err,result) {
					if(err){
						//console.log(err);
						req.flash('error', 'Something went wrong. Please try again later.');
						res.redirect(ADMIN_URL+'projects');		
					}
					else{
							project.deleteProjectUsers(id, function (err,result) {});
							req.flash('success', 'Project deleted successfully.');
							res.redirect(ADMIN_URL+'projects');					
					}				
				});			
	}	
}