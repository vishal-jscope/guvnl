var home = require('../../models/admin/Home');
var async = require('async');
exports.index = function(req,res,next) {
		async.parallel([
		 	function(callback) { home.countRecords('users', callback) },
		 	function(callback) { home.countRecords('teams',callback) },
		  	function(callback) { home.countRecords('projects', callback) }
		], function(err, results) {

			var user_counts = results[0];
			var team_counts = results[1];
			var project_counts = results[2];			
			
			console.log(user_counts);
			res.render('admin/dashboard.ejs',{
				page : 'dashboard',
				user_counts : user_counts,
				team_counts : team_counts,
				project_counts : project_counts
			});
		 
		});				
}

