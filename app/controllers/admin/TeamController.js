var team = require('../../models/admin/Team');
var async = require('async');
exports.index = function(req,res,next) {

		team.getAll(function (err,result) {
			if(err){
				//console.log(err);
				//req.flash('error', 'Something went wrong. Please try again later.');
				res.render('admin/teams/index.ejs',{
					error : req.flash('Something went wrong. Please try again later.')
				});	
			}
			else{
				
				var final_result = [];
					result.forEach(function(data,index){
						final_result[index] = {
												id         : data.id,
												name       : data.name,
												status     : data.status,
												created_at : req.app.locals.moment(data.created_at).format('DD-MM-YYYY'),
												updated_at : req.app.locals.moment(data.updated_at).format('DD-MM-YYYY')
											};

					});
				res.render('admin/teams/index.ejs',{
					page : 'teams',
					result : final_result
				});				
			}					
			
		});			
}

exports.create = function(req,res,next) {
		team.getAllUsers(function (err,result) {
			if(err){
				//console.log(err);
				//req.flash('error', 'Something went wrong. Please try again later.');
				res.render('admin/teams/index.ejs',{
					error : req.flash('Something went wrong. Please try again later.')
				});	
			}
			else{				
				var final_result = [];
					result.forEach(function(data,index){
						final_result[index] = {
											id         : data.id,
											name       : data.name,
											email      : data.email,
										};

					});
				console.log(final_result);	
				res.render('admin/teams/create.ejs',{
					page : 'teams',
					users : final_result
				});				
			}					
			
		});			
		
}

exports.store = function(req,res,next) {
	
	if(req.method == 'POST'){
		if(req.body.submit == 'Add Team'){
	 
				// check name exist
				team.checkNameExist('',req.body.name, function (err,result) {
					if(err){
						//console.log(err);
						req.flash('error','Something went wrong. Please try again later.');
						res.redirect(ADMIN_URL+'teams/create');		
					}
					else{
							if(result.length > 0){
								req.flash('error','Team already exist.');
								res.redirect(ADMIN_URL+'teams/create');								
							}
							else{
								var status = 0;
								if(req.body.status == 'on'){
									status = 1;
								}
								const data = {
												name: req.body.name,
										        status: status,
										        created_at: req.app.locals.moment().format('YYYY-MM-DD h:m:s'),
										        updated_at: req.app.locals.moment().format('YYYY-MM-DD h:m:s')
										    };
								team.store(data, function (err,result) {
									if(err){
										//console.log(err);
										req.flash('error', 'Something went wrong. Please try again later.');
										res.redirect(ADMIN_URL+'teams');		
									}
									else{

											var user_ids = req.body.users;
											
											if(user_ids.length > 0){
												user_ids.forEach(function(user_id,index){
													const  u_data = {
																		team_id: result,
																        user_id: user_id,
																    }
												    team.teamUsersStore(u_data, function (err,result) {});
													
												});
												
											}
											req.flash('success', 'Team added successfully.');
											res.redirect(ADMIN_URL+'teams');					
									}					
									
								});
							}				
					}					
					
				});

		}		
	}	
}

exports.edit = function(req,res,next) {
		var id = req.params.id;
		if(id == 0 || id == ''){
			res.redirect(ADMIN_URL+'teams');
		}

		/*team.get(id, function (err,result) {
			if(err){
				//console.log(err);
				req.flash('error', 'Something went wrong. Please try again later.');
				res.redirect(ADMIN_URL+'teams');		
			}
			else{
									
					var final_result = {};
					result.forEach(function(data,index){
						final_result =  {
											id         : data.id,
											name       : data.name,
											status      : data.status
										};
					});
					
					team.getTeamUsers(id, function (err,result) {

						console.log(final_result);
						console.log(result);
						res.render('admin/teams/edit.ejs',{
						result : final_result,
						selected_result : result
						});

					});											
			}			
		});*/

		async.parallel([
		 	function(callback) { team.get(id, callback) },
		 	function(callback) { team.getAllUsers(callback) },
		  	function(callback) { team.getSelectedTeamUsers(id, callback) }
		], function(err, results) {

			var final_result = [];
			results[0].forEach(function(data,index){
				final_result =  {
									id         : data.id,
									name       : data.name,
									status      : data.status
								};
			});

			var users = [];
			results[1].forEach(function(data,index){
				users[index] = {
									id         : data.id,
									name       : data.name,
									email      : data.email,
								};

			});

			var selected_users = [];
			results[2].forEach(function(data,index){
				selected_users[index] = data.user_id;

			});
			
			console.log(selected_users);
			res.render('admin/teams/edit.ejs',{
						page : 'teams',
						result : final_result,
						users : users,
						selected_users : selected_users
						});
		 
		});	
}


exports.update = function(req,res,next) {
	
	if(req.method == 'PUT'){
		if(req.body.submit == 'Edit Team'){

				var id = req.body.id;

				// check name exist
				team.checkNameExist(id,req.body.name, function (err,result) {
					if(err){
						//console.log(err);
						req.flash('error','Something went wrong. Please try again later.');
						res.redirect(ADMIN_URL+'teams/create');		
					}
					else{
							if(result.length > 0){
								req.flash('error','Team already exist.');
								res.redirect(ADMIN_URL+'teams/edit/'+id);								
							}
							else{
								var status = 0;
								if(req.body.status == 'on'){
									status = 1;
								}
								const data = {
												name: req.body.name,
										        status: status,
										        updated_at: req.app.locals.moment().format('YYYY-MM-DD h:m:s')
										    };
								team.update(data,id, function (err,result) {
									if(err){
										//console.log(err);
										req.flash('error', 'Something went wrong. Please try again later.');
										res.redirect(ADMIN_URL+'teams');		
									}
									else{

											var user_ids = req.body.users;
											
											if(user_ids.length > 0){
												team.deleteTeamUsers(id, function (err,result) {

													user_ids.forEach(function(user_id,index){
													const  u_data = {
																		team_id: id,
																        user_id: user_id,
																    }
												    team.teamUsersStore(u_data, function (err,result) {});
													
													});
												});
												
												
											}
											req.flash('success', 'Team updated successfully.');
											res.redirect(ADMIN_URL+'teams');					
									}					
									
								});
							}				
					}					
					
				});


		}	
	}	
}

exports.delete = function(req,res,next) {
		console.log(req.body);
	if(req.method == 'DELETE'){
		
				var id = req.body.id;
				team.delete(id, function (err,result) {
					if(err){
						//console.log(err);
						req.flash('error', 'Something went wrong. Please try again later.');
						res.redirect(ADMIN_URL+'teams');		
					}
					else{
							team.deleteTeamUsers(id, function (err,result) {});
							req.flash('success', 'Team deleted successfully.');
							res.redirect(ADMIN_URL+'teams');					
					}					
					
				});
			
	}	
}